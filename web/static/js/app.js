// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
// import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"
import React from 'react';
import ReactDOM from 'react-dom';

// Simple hello world example
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);

function formatName(user) {
  return user.firstName + " " + user.lastName;
}

const user = {
  firstName: "Ranjani",
  lastName: "Hathaway"
}

const element = (
  <h1>
    Hello, {formatName(user)}!
  </h1>
)

ReactDOM.render(
  element,
  document.getElementById("sample1")
);

// Clock example
function tick() {
  const element = (
    <div>
      <h1>Hello world!</h1>
      <h2>It is now {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
  ReactDOM.render(
    element,
    document.getElementById("sample2")
  );
}

setInterval(tick, 1000);

// Welcome message, using React components
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

const element2=<Welcome name="Charles" />;
ReactDOM.render(
  element2,
  document.getElementById("sample3")
);

// Clock example, as a reusable component
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  componentDidMount() {
    this.timerId = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    )
  }
}

ReactDOM.render(
  <Clock />,
  document.getElementById("sample4")
)

// Toggle example
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};
    // JS is dynamic scoped, but this forces this to tbe class object in callbacks
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    // Notice use of lambda to access state; you shouldn't access this.state since
    //  setState may be done async to keep things fast
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        {this.state.isToggleOn ? "ON" : "OFF"}
      </button>
    )
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById("sample5")
)
